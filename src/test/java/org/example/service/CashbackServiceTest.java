package org.example.service;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CashbackServiceTest {
    @Test
    void shouldCalculate() {
        final CashbackService service = new CashbackService();
        final int amount = 532_25;
        final int expected = 5_00;

        final int actual = service.calculate(amount);

        assertEquals(expected, actual);
    }

    @Test
    void shouldReturnMaxValue() {
        final CashbackService service = new CashbackService();
        final int amount = 15_000_000_00;
        final int expected = 3000_00;

        final int actual = service.calculate(amount);

        assertEquals(expected, actual);
    }
}
