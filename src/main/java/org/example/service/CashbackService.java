package org.example.service;

public class CashbackService {
    /**
     * <p>Calculates cashback (in kopecks) from the amount of transactions (in kopecks) for the billing period.</p>
     * @param amount Amount of transactions (in kopecks) for the billing period
     */
    public int calculate(int amount) {
        final int maxCashback = 3000_00;
        int cashback = (amount/100_00)*1_00;
        if (cashback > maxCashback) {
            return maxCashback;
        }
        return cashback;
    }
}
